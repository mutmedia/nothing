import React from 'react'
import _ from 'lodash'

function Conversions (props) {
  const verbElements = _.map(props.conversions, (word, conversion) => (
    <div key={`conversion_${i}`}>
      <span />
    </div>
  ))

  return (
    <div>
      <h2>Conversions</h2>
      {verbElements}
    </div>
  )
}

export default Conversions
