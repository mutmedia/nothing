import React from 'react'
import _ from 'lodash'

function Verbs (props) {
  const verbs = _.filter(_.keys(props.meanings), (word) => _.startsWith(word, 'to'))
  const verbElements = _.map(verbs, (verb, i) => (
    <div key={`verb_${i}`}>
      <span>{verb}</span>
    </div>
  ))

  return (
    <div>
      <h2>Verbs</h2>
      {verbElements}
    </div>
  )
}

export default Verbs
