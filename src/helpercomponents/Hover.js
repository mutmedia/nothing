import React from 'react'

const Hover = ({ onHover, children }) => (
  <span className='hover'>
    <span className='hover--off'>{children}</span>
    <span className='hover--on'>{onHover}</span>
  </span>
)

export default Hover
